package entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Fredrik on 08.02.2016.
 */
@Entity
@SequenceGenerator(name = Vote.SEQUENCE_NAME, sequenceName = Vote.SEQUENCE_NAME, initialValue = 50)
public class Vote {
    public static final String SEQUENCE_NAME = "Vote_sequence";
    public enum Type {UP, DOWN}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    private long id;

    @ManyToOne
    private News upVotedNews;

    @ManyToOne
    private News downVotedNews;

    @ManyToOne
    private Comment upVotedComment;

    @ManyToOne
    private Comment downVotedComment;

    @NotNull
    private Date createdAt;

    @ManyToOne
    @NotNull
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public News getUpVotedNews() {
        return upVotedNews;
    }

    public void setUpVotedNews(News upVoteNews) {
        this.upVotedNews = upVoteNews;
    }

    public News getDownVotedNews() {
        return downVotedNews;
    }

    public void setDownVotedNews(News downVoteNews) {
        this.downVotedNews = downVoteNews;
    }

    public Comment getUpVotedComment() {
        return upVotedComment;
    }

    public void setUpVotedComment(Comment upVotedComment) {
        this.upVotedComment = upVotedComment;
    }

    public Comment getDownVotedComment() {
        return downVotedComment;
    }

    public void setDownVotedComment(Comment downVotedComment) {
        this.downVotedComment = downVotedComment;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
