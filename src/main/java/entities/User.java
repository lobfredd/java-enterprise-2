package entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * Created by Fredrik on 25.01.2016.
 */
@Entity
@SequenceGenerator(name = User.SEQUENCE_NAME, sequenceName = User.SEQUENCE_NAME, initialValue = 50)
@NamedQueries({
        @NamedQuery(name = User.GET_ALL_USERS, query = "select user from User user"),
        @NamedQuery(name = User.GET_BY_EMAIL, query = "select user from User user where user.email = :" + User.EMAIL_PARAM),
        @NamedQuery(name = User.GET_BY_USERNAME, query = "select user from User user where user.username = :" + User.USERNAME_PARAM)
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
    public static final String SEQUENCE_NAME = "User_sequence";
    public static final String GET_ALL_USERS = "User.getAll";
    public static final String GET_BY_EMAIL = "User.getByEmail";
    public static final String EMAIL_PARAM = "email";
    public static final String GET_BY_USERNAME = "User.getByUsername";
    public static final String USERNAME_PARAM = "username";

    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    private long id;

    @XmlTransient
    @NotNull
    @Size(min = 3, max = 255)
    private String email;

    @NotNull
    @Size(min = 3, max = 100)
    private String username;

    @XmlTransient
    @NotNull
    private String hash;

    @XmlTransient
    @NotNull
    private String salt;

    private String country;

    @XmlTransient
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "author")
    private List<Comment> comments;

    @XmlTransient
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "author")
    private List<News> newsList;

    @XmlTransient
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user")
    private List<Vote> votes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String password) {
        this.hash = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<News> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<News> news) {
        this.newsList = news;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }
}
