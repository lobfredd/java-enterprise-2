package entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Fredrik on 25.01.2016.
 */

@Entity
@SequenceGenerator(name = News.SEQUENCE_NAME, sequenceName = News.SEQUENCE_NAME, initialValue = 50)
@NamedQueries({
        @NamedQuery(name = News.GET_ALL, query = "select news from News news"),
        @NamedQuery(name = News.GET_TOTAL_COUNT, query = "select count(news) from News news"),
        @NamedQuery(name = News.GET_ALL_NEWEST_FIRST, query = "SELECT news FROM News news ORDER BY news.createdAt DESC"),
        @NamedQuery(name = News.GET_MOST_UP_VOTES_OF_TODAY, query = "SELECT news FROM News news WHERE news.createdAt between :" + News.START_OF_TODAY_PARAM + " and :" + News.END_OF_TODAY_PARAM + " ORDER BY news.upVotes.size DESC"),
        @NamedQuery(name = News.GET_MOST_COMMENTS_OF_TODAY, query = "SELECT news FROM News news WHERE news.createdAt between :" + News.START_OF_TODAY_PARAM + " and :" + News.END_OF_TODAY_PARAM + " ORDER BY news.comments.size DESC"),
        @NamedQuery(name = News.GET_HOTTEST, query = "SELECT news FROM News news WHERE news.createdAt between :" + News.START_OF_TODAY_PARAM + " and :" + News.END_OF_TODAY_PARAM + " ORDER BY news.upVotes.size DESC, news.comments.size DESC"),
        @NamedQuery(name = News.GET_TOP, query = "SELECT news FROM News news ORDER BY news.upVotes.size DESC, news.comments.size DESC")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class News {
    public static final String SEQUENCE_NAME = "News_sequence";
    public static final String GET_ALL = "News.getAll";
    public static final String GET_TOTAL_COUNT = "News.getTotalCount";
    public static final String GET_ALL_NEWEST_FIRST = "News.getAllNewestFirst";
    public static final String GET_MOST_UP_VOTES_OF_TODAY = "News.getMostVotesOfToday";
    public static final String GET_MOST_COMMENTS_OF_TODAY = "News.getMostCommentsOfToday";
    public static final String GET_HOTTEST = "News.getHottest";
    public static final String GET_TOP = "News.getTop";

    public static final String START_OF_TODAY_PARAM = "startOfToday";
    public static final String END_OF_TODAY_PARAM = "endOfToday";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    private long id;

    @NotNull
    @Size(min = 1, max = 1000)
    private String title;

    @NotNull
    @Size(min = 1, max = 90000)
    private String text;

    @NotNull
    @ManyToOne
    private User author;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @XmlElementWrapper(name = "comments")
    @XmlElement(name = "comment")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "news")
    private Set<Comment> comments;

    @XmlTransient
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "upVotedNews", orphanRemoval = true)
    private List<Vote> upVotes;

    @XmlTransient
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "downVotedNews", orphanRemoval = true)
    private List<Vote> downVotes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public List<Vote> getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(List<Vote> upVotes) {
        this.upVotes = upVotes;
    }

    public List<Vote> getDownVotes() {
        return downVotes;
    }

    public void setDownVotes(List<Vote> downVotes) {
        this.downVotes = downVotes;
    }

    public void removeAllUpVotesBy(User user){
        getUpVotes().removeIf(v -> v.getUser().getId() == user.getId());
    }

    public void removeAllDownVotesBy(User user){
        getDownVotes().removeIf(v -> v.getUser().getId() == user.getId());
    }

    public boolean isPrevUpVotedBy(User user){
        return getUpVotes().stream()
                .filter(v -> v.getUser().getId() == user.getId())
                .findFirst()
                .orElse(null) != null;
    }

    public boolean isPrevDownVotedBy(User user){
        return getDownVotes().stream()
                .filter(v -> v.getUser().getId() == user.getId())
                .findFirst()
                .orElse(null) != null;
    }
}
