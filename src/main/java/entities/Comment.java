package entities;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Fredrik on 25.01.2016.
 */

@Entity
@SequenceGenerator(name = Comment.SEQUENCE_NAME, sequenceName = Comment.SEQUENCE_NAME, initialValue = 50)
@NamedQueries({
        @NamedQuery(name = Comment.GET_ALL_COMMENTS, query = "select comment from Comment comment"),
        @NamedQuery(name = Comment.GET_TOTAL_COUNT, query = "select count(comment) from Comment comment")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Comment {
    public static final String SEQUENCE_NAME = "Comment_sequence";
    public static final String GET_ALL_COMMENTS = "Comment.getAll";
    public static final String GET_TOTAL_COUNT = "Comment.getTotalCount";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    private long id;

    @NotNull
    @NotBlank
    private String text;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @NotNull
    @ManyToOne
    private User author;

    @XmlTransient
    @ManyToOne
    private News news;

    @XmlTransient
    @ManyToOne
    private Comment parentComment;

    @XmlElementWrapper(name = "comments")
    @XmlElement(name = "comment")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "parentComment")
    private Set<Comment> comments;

    @XmlTransient
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "upVotedComment")
    private List<Vote> upVotes;

    @XmlTransient
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "downVotedComment")
    private List<Vote> downVotes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Vote> getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(List<Vote> upVotes) {
        this.upVotes = upVotes;
    }

    public List<Vote> getDownVotes() {
        return downVotes;
    }

    public void setDownVotes(List<Vote> downVotes) {
        this.downVotes = downVotes;
    }

    public Comment getParentComment() {
        return parentComment;
    }

    public void setParentComment(Comment comment) {
        this.parentComment = comment;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
}
