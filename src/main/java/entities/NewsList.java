package entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Fredrik on 04.04.2016.
 */
@XmlRootElement
@XmlSeeAlso(News.class)
public class NewsList extends ArrayList<News> {

    public NewsList() {
        super();
    }

    public NewsList(Collection<? extends News> c) {
        super(c);
    }

    @XmlElement(name = "news")
    public List<News> getNews() {
        return this;
    }
    public void setNews(List<News> news) {
        this.addAll(news);
    }
}
