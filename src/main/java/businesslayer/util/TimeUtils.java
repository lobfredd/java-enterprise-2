package businesslayer.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Fredrik on 29.02.2016.
 */
public class TimeUtils {

    public static Date getStartOfToday(){
        return getDayOf(new Date());
    }

    public static Date getEndOfToday(){
       return getEndOfDayOf(new Date());
    }

    public static Date getEndOfDayOf(Date date){
        Calendar endOfDay = Calendar.getInstance();
        endOfDay.setTime(date);
        endOfDay.set(Calendar.HOUR_OF_DAY, 23);
        endOfDay.set(Calendar.MINUTE, 59);
        endOfDay.set(Calendar.SECOND, 59);
        endOfDay.set(Calendar.MILLISECOND, 999);
        return endOfDay.getTime();
    }

    public static Date getDayOf(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
