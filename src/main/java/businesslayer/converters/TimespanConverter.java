package businesslayer.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.Date;

/**
 * Created by Fredrik on 22.03.2016.
 */
@FacesConverter("DurationSinceNowConverter")
public class TimespanConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if(o == null) return "";
        if(!(o instanceof Date)) return "";

        long time = ((Date)o).getTime();
        long durationInSec = (new Date().getTime() - time) / 1000;

        int days = (int) (durationInSec / 86400);
        int hours = (int) (durationInSec / 3600) % 24;
        int mins = (int) (durationInSec / 60) % 60;

        if(days > 0) return String.format("%d days", days);
        else if(hours > 0) return String.format("%d hours", hours);
        else if(mins > 0) return String.format("%d minutes", mins);
        else return String.format("%d seconds", durationInSec);
    }
}
