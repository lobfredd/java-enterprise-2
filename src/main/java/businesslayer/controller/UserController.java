package businesslayer.controller;

import businesslayer.ejb.UserEJB;
import entities.User;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Fredrik on 23.02.2016.
 */
@Named
@SessionScoped
public class UserController implements Serializable {
    private User user;
    private String password;
    private String repeatedPassword;
    private String emailOrUsername;
    private boolean loggedIn;

    @Inject
    private UserEJB userEJB;

    @PostConstruct
    public void construct(){
        user = new User();
    }

    public void logOut(){
        user = new User();
        loggedIn = false;
    }

    public void logIn(){
        FacesMessage message;

        User tmpUser = userEJB.logIn(emailOrUsername, password);
        if(tmpUser != null) {
            emailOrUsername = null;
            password = null;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", "");
            loggedIn = true;
        }
        else {
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid credentials", "");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        RequestContext.getCurrentInstance().addCallbackParam("loggedIn", loggedIn);
    }

    public void registerUser(){
        FacesMessage message = null;
        boolean registerSucceded = false;

        if(!password.equals(repeatedPassword)){
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Passwords does not match", "");
        }
        else{
            User tmpUser = userEJB.registerUser(user, password);
            if(tmpUser != null){
                user = tmpUser;
                registerSucceded = true;
                loggedIn = true;
                password = null;
                repeatedPassword = null;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", "");
            }
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        RequestContext.getCurrentInstance().addCallbackParam("registerSucceeded", registerSucceded);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailOrUsername() {
        return emailOrUsername;
    }

    public void setEmailOrUsername(String emailOrUsername) {
        this.emailOrUsername = emailOrUsername;
    }

    public String getRepeatedPassword() {
        return repeatedPassword;
    }

    public void setRepeatedPassword(String repeatedPassword) {
        this.repeatedPassword = repeatedPassword;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
}
