package businesslayer.controller;

import businesslayer.ejb.CommentEJB;
import businesslayer.ejb.NewsEJB;
import entities.Comment;
import entities.News;
import entities.User;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Fredrik on 14.03.2016.
 */
@Named
@ViewScoped
public class NewsController implements Serializable {
    @Inject
    private NewsEJB newsEJB;
    @Inject
    private CommentEJB commentEJB;

    private News news;
    private Comment comment;
    private List<News> currentNewsList;
    private Comment subComment;

    @PostConstruct
    private void construct(){
        news = new News();
        comment = new Comment();
        subComment = new Comment();
        currentNewsList = new ArrayList<>();
    }

    public void fetchHottestNews(){
        currentNewsList = newsEJB.getHottestNews();
    }

    public void fetchNewestNews(){
        currentNewsList = newsEJB.getAllNewestFirst();
    }

    public void fetchTopNews(){
        currentNewsList = newsEJB.getTopNews();
    }

    public void saveNews(User author){
        news.setAuthor(author);
        news.setCreatedAt(new Date());
        news = newsEJB.persist(news);

        if(news.getId() <= 0){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ""));
        }
        RequestContext.getCurrentInstance().addCallbackParam("newsSaved", news.getId() > 0);
    }

    public void loadNewsById(){
        news = newsEJB.getById(news.getId());
    }

    public String postComment(User user){
        comment.setCreatedAt(new Date());
        comment.setAuthor(user);
        comment.setNews(news);
        commentEJB.persist(comment);

        return "news-detail.xhtml?faces-redirect=true&includeViewParams=true";
    }

    public void voteUpNews(int newsId, User user){
        if(user == null) return;

        int index = getIndexByNewsId(newsId);
        if(index >= 0) currentNewsList.set(index, newsEJB.upVote(currentNewsList.get(index), user));
        else news = newsEJB.upVote(news, user);
    }

    public void voteDownNews(int newsId, User user){
        if(user == null) return;

        int index = getIndexByNewsId(newsId);
        if(index >= 0) currentNewsList.set(index, newsEJB.downVote(currentNewsList.get(index), user));
        else news = newsEJB.downVote(news, user);
    }

    public void voteUpComment(int commentId, User user){
        if(user == null) return;

    }

    public void voteDownComment(int commentId, User user){
        if(user == null) return;

    }

    public String postSubComment(int parentId, User user){
        Comment parent = commentEJB.getById(parentId);
        subComment.setCreatedAt(new Date());
        subComment.setAuthor(user);
        subComment.setParentComment(parent);
        commentEJB.persist(subComment);
        return "news-detail.xhtml?faces-redirect=true&includeViewParams=true";
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<News> getCurrentNewsList() {
        return currentNewsList;
    }

    public void setCurrentNewsList(List<News> currentNewsList) {
        this.currentNewsList = currentNewsList;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Comment getSubComment() {
        return subComment;
    }

    public void setSubComment(Comment subComment) {
        this.subComment = subComment;
    }

    private int getIndexByNewsId(int newsId){
        int index = -1;
        for (int i = 0; i < currentNewsList.size(); i++) {
            if (currentNewsList.get(i).getId() == newsId){
                index = i;
                break;
            }
        }
        return index;
    }
}
