package businesslayer.rest;

import businesslayer.ejb.NewsEJB;
import entities.News;
import entities.NewsList;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Created by Fredrik on 03.04.2016.
 */
@Path("news")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class NewsService {

    @Inject
    private NewsEJB newsEJB;

    @Context
    private UriInfo uriInfo;

    @GET
    public Response getAllNews(){
        NewsList newsList = new NewsList(newsEJB.getAllNewestFirst());
        return Response.ok(newsList).build();
    }

    @GET
    @Path("/hottest")
    public Response getHottestNews(){
        NewsList newsList = new NewsList(newsEJB.getHottestNews());
        return Response.ok(newsList).build();
    }

    @GET
    @Path("{id: \\d+}")
    public News getNewsById(@PathParam("id") long newsId){
        News news = newsEJB.getById(newsId);
        if(news == null){
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return news;
    }
}
