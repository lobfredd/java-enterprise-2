package businesslayer.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Fredrik on 03.04.2016.
 */
@ApplicationPath("/api")
public class ApplicationConfig extends Application {
    private final Set<Class<?>> classes;

    public ApplicationConfig(){
        classes = new HashSet<>();
        classes.add(NewsService.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }
}
