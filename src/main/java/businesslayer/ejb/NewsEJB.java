package businesslayer.ejb;

import businesslayer.util.TimeUtils;
import entities.News;
import entities.User;
import entities.Vote;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Fredrik on 07.02.2016.
 */
@Stateless
public class NewsEJB {

    @PersistenceContext(unitName = "PG6100")
    private EntityManager entityManager;

    public NewsEJB(){
    }

    NewsEJB(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public News persist(News news){
        entityManager.persist(news);
        return news;
    }

    public Object merge(Object obj){
        if(!entityManager.contains(obj)){
            obj = entityManager.merge(obj);
        }
        return obj;
    }

    public News upVote(News news, User user){
        if(news == null || user == null) return news;
        if(news.isPrevUpVotedBy(user)) return news;

        news = (News) merge(news);
        user = (User) merge(user);
        news.removeAllDownVotesBy(user);

        Vote upVote = new Vote();
        upVote.setCreatedAt(new Date());
        upVote.setUpVotedNews(news);
        upVote.setUser(user);
        news.getUpVotes().add(upVote);
        entityManager.persist(upVote);

        return news;
    }

    public News downVote(News news, User user){
        if(news == null || user == null) return news;
        if(news.isPrevDownVotedBy(user)) return news;

        news = (News) merge(news);
        user = (User) merge(user);
        news.removeAllUpVotesBy(user);

        Vote downVote = new Vote();
        downVote.setCreatedAt(new Date());
        downVote.setDownVotedNews(news);
        downVote.setUser(user);
        news.getDownVotes().add(downVote);
        entityManager.persist(downVote);

        return news;
    }

    public List<News> getAll() {
        return entityManager.createNamedQuery(News.GET_ALL, News.class).getResultList();
    }

    public List<News> getAllNewestFirst() {
        return entityManager.createNamedQuery(News.GET_ALL_NEWEST_FIRST, News.class).getResultList();
    }

    public List<News> getHottestNews() {
        return entityManager.createNamedQuery(News.GET_HOTTEST, News.class)
                .setParameter(News.START_OF_TODAY_PARAM, TimeUtils.getStartOfToday(), TemporalType.TIMESTAMP)
                .setParameter(News.END_OF_TODAY_PARAM, TimeUtils.getEndOfToday(), TemporalType.TIMESTAMP)
                .getResultList();
    }

    public List<News> getTopNews() {
        return entityManager.createNamedQuery(News.GET_TOP, News.class).getResultList();
    }

    public List<News> mostUpVotesOfToday(){
        return entityManager.createNamedQuery(News.GET_MOST_UP_VOTES_OF_TODAY, News.class)
                .setParameter(News.START_OF_TODAY_PARAM, TimeUtils.getStartOfToday(), TemporalType.TIMESTAMP)
                .setParameter(News.END_OF_TODAY_PARAM, TimeUtils.getEndOfToday(), TemporalType.TIMESTAMP)
                .getResultList();
    }

    public List<News> mostCommentsOfToday(){
        return entityManager.createNamedQuery(News.GET_MOST_COMMENTS_OF_TODAY, News.class)
                .setParameter(News.START_OF_TODAY_PARAM, TimeUtils.getStartOfToday(), TemporalType.TIMESTAMP)
                .setParameter(News.END_OF_TODAY_PARAM, TimeUtils.getEndOfToday(), TemporalType.TIMESTAMP)
                .getResultList();
    }

    public int totalNumberOfNews(){
        return ((Number)entityManager.createNamedQuery(News.GET_TOTAL_COUNT).getSingleResult()).intValue();
    }

    public double getAvgNewsPerDay(){
        List<News> newsList = getAll();
        Set<Date> days = new HashSet<Date>();

        for (News news : newsList){
            Date dayOf = TimeUtils.getDayOf(news.getCreatedAt());
            days.add(dayOf);
        }

        return newsList.size() / (double)days.size();
    }

    public News getById(long id) {
        return entityManager.find(News.class, id);
    }
}
