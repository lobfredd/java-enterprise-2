package businesslayer.ejb;

import entities.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

/**
 * Created by Fredrik on 08.02.2016.
 */
@Stateless
public class UserEJB implements Serializable{

    @PersistenceContext(unitName = "PG6100")
    private EntityManager entityManager;

    public UserEJB(){
    }

    UserEJB(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public User logIn(String emailOrUsername, String password){
        if(emailOrUsername == null || password == null || password.isEmpty() || emailOrUsername.isEmpty()){
            return null;
        }

        User user = getByUsername(emailOrUsername);
        if(user == null){
            user = getByEmail(emailOrUsername);
            if(user == null) return null;
        }

        String hash = DigestUtils.sha256Hex(password + user.getSalt());
        if(user.getHash().equals(hash)){
            return user;
        }
        return null;
    }

    public User registerUser(User user, String password){
        if(user == null || password == null || password.isEmpty()){
            return null;
        }

        if(usernameOrEmailTaken(user)) return user;

        String salt = makeSalt();
        user.setSalt(salt);
        user.setHash(DigestUtils.sha256Hex(password + salt));

        return persist(user);
    }

    public User persist(User user) {
        entityManager.persist(user);
        return user;
    }

    public List<User> getAll() {
        return entityManager.createNamedQuery(User.GET_ALL_USERS, User.class).getResultList();
    }

    public User getById(long id) {
        return entityManager.find(User.class, id);
    }

    public User getByUsername(String username){
        if(username == null || username.isEmpty()) return null;
        try{
            return entityManager.createNamedQuery(User.GET_BY_USERNAME, User.class)
                    .setParameter(User.USERNAME_PARAM, username)
                    .getSingleResult();
        }catch (NoResultException nre){
            return null;
        }
    }

    public User getByEmail(String email){
        if(email == null || email.isEmpty()) return null;
        try{
            return entityManager.createNamedQuery(User.GET_BY_EMAIL, User.class)
                    .setParameter(User.EMAIL_PARAM, email)
                    .getSingleResult();
        } catch (NoResultException nre){
            return null;
        }
    }

    private boolean usernameOrEmailTaken(User user){
        User userWithUsername = getByUsername(user.getUsername());
        if(userWithUsername != null) return true;

        User userWithEmail = getByEmail(user.getEmail());
        return userWithEmail != null;
    }

    private String makeSalt(){
        SecureRandom random = new SecureRandom();
        int bitsPerChar = 5;
        int twoPowerOfBits = 32; // 2^5
        int n = 26;
        assert n * bitsPerChar >= 128;

        return new BigInteger(n * bitsPerChar, random).toString(twoPowerOfBits);
    }
}
