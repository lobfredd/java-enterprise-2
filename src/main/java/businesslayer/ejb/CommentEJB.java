package businesslayer.ejb;

import businesslayer.util.TimeUtils;
import entities.Comment;
import entities.User;
import entities.Vote;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Fredrik on 08.02.2016.
 */
@Stateless
public class CommentEJB {

    @PersistenceContext(unitName = "PG6100")
    private EntityManager entityManager;

    public CommentEJB(){
    }

    CommentEJB(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public Comment persist(Comment comment) {
        entityManager.persist(comment);
        return comment;
    }

    public Comment merge(Comment comment){
        if(!entityManager.contains(comment)){
            comment = entityManager.merge(comment);
        }
        return comment;
    }

    public List<Comment> getAll() {
        return entityManager.createNamedQuery(Comment.GET_ALL_COMMENTS, Comment.class).getResultList();
    }

    public Comment getById(long id) {
        return entityManager.find(Comment.class, id);
    }

    public void upVote(Comment comment, User user){
        if(comment == null) return;

        Vote upVote = new Vote();
        upVote.setCreatedAt(new Date());
        upVote.setUpVotedComment(comment);
        upVote.setUser(user);
        comment.getUpVotes().add(upVote);
        entityManager.persist(upVote);
        merge(comment);
    }

    public void downVote(Comment comment, User user){
        if(comment == null) return;

        Vote downVote = new Vote();
        downVote.setCreatedAt(new Date());
        downVote.setDownVotedComment(comment);
        downVote.setUser(user);
        comment.getDownVotes().add(downVote);
        entityManager.persist(downVote);
        merge(comment);
    }

    public double getAvgCommentsPerDay() {
        List<Comment> comments = getAll();
        Set<Date> days = new HashSet<Date>();

        for (Comment comment : comments){
            Date dayOf = TimeUtils.getDayOf(comment.getCreatedAt());
            days.add(dayOf);
        }

        return comments.size() / (double)days.size();
    }
}
