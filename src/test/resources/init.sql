-- NewsEJBIT
INSERT INTO User(id, email,username, hash, salt) VALUES(1, 'f@f1.com', 'aUsername', 'aHash', 'theSalt');

INSERT INTO News(id, title, text, author_id, createdAt) VALUES(1, 'news1 1', 'test news content', 1, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'));
INSERT INTO News(id, title, text, author_id, createdAt) VALUES(2, 'news1 2', 'test news content 2', 1, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'));
INSERT INTO News(id, title, text, author_id, createdAt) VALUES(3, 'news1 3', 'test news content 3', 1, parseDateTime('14/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'));
INSERT INTO News(id, title, text, author_id, createdAt) VALUES(4, 'news1 4', 'test news content 4', 1, parseDateTime('14/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'));

INSERT INTO Comment(id, text, createdAt, author_id, news_id) VALUES(1, 'comment 1', parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1, 1);
INSERT INTO Comment(id, text, createdAt, author_id, news_id) VALUES(2, 'comment 2', parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1, 1);
INSERT INTO Comment(id, text, createdAt, author_id, news_id) VALUES(3, 'comment 3', parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1, 1);
INSERT INTO Comment(id, text, createdAt, author_id, news_id) VALUES(4, 'comment 4', parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1, 1);

INSERT INTO Vote(id, downVotedNews_id, createdAt, USER_ID) VALUES (1, 1, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1);
INSERT INTO Vote(id, downVotedNews_id, createdAt, USER_ID) VALUES (2, 1, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1);
INSERT INTO Vote(id, upVotedNews_id, createdAt, USER_ID) VALUES (3, 1, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1);
INSERT INTO Vote(id, upVotedNews_id, createdAt, USER_ID) VALUES (4, 1, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1);


-- UserEJBIT
-- Password is: testPassword
INSERT INTO User(id, email, username, hash, salt) VALUES(2, 'f@f2.com', 'a2Username', '4c04b4bd1c7b2462aa017926f0cf80598afbe69b28d04f7d61af32708ea5c6eb', '6m45rjhkgh05cmjdsm93ooefu4');

INSERT INTO News(id, title, text, author_id, createdAt) VALUES(5, 'news1 5', 'test news content 5', 2, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'));

INSERT INTO Comment(id, text, createdAt, author_id, news_id) VALUES(5, 'comment 5', parseDateTime('13/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 2, 5);
INSERT INTO Comment(id, text, createdAt, author_id, news_id) VALUES(6, 'comment 6', parseDateTime('13/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 2, 5);


-- CommentEJBIT
INSERT INTO User(id, email,username, hash, salt) VALUES(3, 'f@f3.com', 'a3Username', 'aHash', 'theSalt');
INSERT INTO News(id, title, text, author_id, createdAt) VALUES(6, 'news1 6', 'test news content 6', 3, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'));

INSERT INTO Comment(id, text, createdAt, author_id, news_id) VALUES(7, 'comment 7', parseDateTime('13/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 3, 6);
INSERT INTO Comment(id, text, createdAt, author_id, parentComment_id) VALUES(8, 'comment 8', parseDateTime('13/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 3, 7);
INSERT INTO Comment(id, text, createdAt, author_id, parentComment_id) VALUES(9, 'comment 9', parseDateTime('13/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 3, 7);


INSERT INTO Vote(id, downVotedComment_id, createdAt, USER_ID) VALUES (5, 7, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1);
INSERT INTO Vote(id, downVotedComment_id, createdAt, USER_ID) VALUES (6, 7, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1);
INSERT INTO Vote(id, upVotedComment_id, createdAt, USER_ID) VALUES (7, 7, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1);
INSERT INTO Vote(id, upVotedComment_id, createdAt,USER_ID) VALUES (8, 7, parseDateTime('12/10/2015 12:00:31' ,'dd/MM/yyyy hh:mm:ss'), 1);