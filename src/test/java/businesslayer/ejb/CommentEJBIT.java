package businesslayer.ejb;

import entities.Comment;
import entities.User;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Fredrik on 15.02.2016.
 */
public class CommentEJBIT extends TestSuite {

    private static final int sampleCommentId = 7;
    private static final int sampleUserId = 3;

    @Test
    public void persist(){
        Comment comment = new Comment();
        User user = userEJB.getById(sampleUserId);
        comment.setAuthor(user);
        comment.setText("test comment");
        comment.setCreatedAt(new Date());

        persistCommentInTransaction(comment);

        assertTrue(comment.getId() > 0);
    }

    @Test
    public void getCommentById(){
        Comment comment = commentEJB.getById(sampleCommentId);
        assertNotNull(comment);
        assertEquals("comment 7", comment.getText());
    }

    @Test
    public void getAll(){
        List<Comment> comments = commentEJB.getAll();
        assertEquals(9, comments.size());
    }

    @Test
    public void hasComments(){
        Comment comment = commentEJB.getById(sampleCommentId);
        assertEquals(2, comment.getComments().size());
    }

    @Test
    public void addComment(){
        Comment comment = commentEJB.getById(sampleCommentId);
        int subCommentCount = comment.getComments().size();

        Comment subComment = new Comment();
        subComment.setText("some text");
        subComment.setCreatedAt(new Date());
        subComment.setAuthor(userEJB.getById(sampleUserId));
        comment.getComments().add(subComment);

        transaction.begin();
        commentEJB.persist(subComment);
        commentEJB.persist(comment);
        transaction.commit();

        comment = commentEJB.getById(sampleCommentId);
        assertEquals(subCommentCount+1, comment.getComments().size());
    }

    @Test
    public void removeComment(){
        Comment comment = commentEJB.getById(sampleCommentId);
        int subCommentCount = comment.getComments().size();
        comment.getComments().remove(comment.getComments().iterator().next());

        persistCommentInTransaction(comment);

        comment = commentEJB.getById(sampleCommentId);
        assertEquals(subCommentCount-1, comment.getComments().size());
    }

    @Test
    public void hasVotes(){
        Comment comment = commentEJB.getById(sampleCommentId);
        assertEquals(2, comment.getUpVotes().size());
        assertEquals(2, comment.getDownVotes().size());
    }

    @Test
    public void addVotes(){
        User user = userEJB.getById(sampleUserId);
        Comment comment = commentEJB.getById(sampleCommentId);
        int upVoteCount = comment.getUpVotes().size();
        int downVoteCount = comment.getDownVotes().size();

        transaction.begin();
        commentEJB.upVote(comment, user);
        commentEJB.downVote(comment, user);
        transaction.commit();

        comment = commentEJB.getById(sampleCommentId);
        assertEquals(upVoteCount+1, comment.getUpVotes().size());
        assertEquals(downVoteCount+1, comment.getDownVotes().size());
    }

    @Test
    public void removeVotes(){
        Comment comment = commentEJB.getById(sampleCommentId);
        int upVoteCount = comment.getUpVotes().size();
        int downVoteCount = comment.getDownVotes().size();
        comment.getUpVotes().remove(0);
        comment.getDownVotes().remove(0);

        persistCommentInTransaction(comment);

        comment = commentEJB.getById(sampleCommentId);
        assertEquals(upVoteCount-1, comment.getUpVotes().size());
        assertEquals(downVoteCount-1, comment.getDownVotes().size());
    }

    @Test
    public void getAvgCommentsPerDay(){
        double avg = commentEJB.getAvgCommentsPerDay();
        assertEquals(4.5, avg, 0.009);
    }

    private void persistCommentInTransaction(Comment comment){
        transaction.begin();
        commentEJB.persist(comment);
        transaction.commit();
    }
}
