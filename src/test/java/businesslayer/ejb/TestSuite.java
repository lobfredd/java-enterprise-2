package businesslayer.ejb;

import org.junit.After;
import org.junit.Before;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.validation.ConstraintViolationException;

/**
 * Created by Fredrik on 15.02.2016.
 */
public class TestSuite {
    private EntityManagerFactory factory;
    private EntityManager entityManager;

    protected EntityTransaction transaction;
    protected NewsEJB newsEJB;
    protected UserEJB userEJB;
    protected CommentEJB commentEJB;

    @Before
    public void setUp() throws Exception {
        factory = Persistence.createEntityManagerFactory("PG6100");
        entityManager = factory.createEntityManager();
        transaction = entityManager.getTransaction();
        newsEJB = new NewsEJB(entityManager);
        userEJB = new UserEJB(entityManager);
        commentEJB = new CommentEJB(entityManager);
    }

    @After
    public void tearDown() throws Exception {
        entityManager.close();
        factory.close();
    }

    protected ConstraintViolationException unwrapConstraintViolationException(Throwable t){
        while (t != null && t.getClass() != ConstraintViolationException.class){
            t = t.getCause();
        }
        return (ConstraintViolationException) t;
    }
}
