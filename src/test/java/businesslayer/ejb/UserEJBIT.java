package businesslayer.ejb;

import entities.Comment;
import entities.News;
import entities.User;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Fredrik on 15.02.2016.
 */
public class UserEJBIT extends TestSuite {

    private static final int sampleUserId = 2;
    private static final String email = "f@f2.com";
    private static final String username = "a2Username";
    private static final String password = "testPassword";

    @Test
    public void persist(){
        User user = new User();
        user.setEmail("test@g.com");
        user.setSalt("someRandomSalt");
        user.setHash("aTestPassword");
        user.setUsername("testUsername");

        persistUserInTransaction(user);

        assertTrue(user.getId() > 0);
    }

    @Test
    public void getUser(){
        User user = userEJB.getById(sampleUserId);
        assertNotNull(user);

        user = userEJB.getById(-1);
        assertNull(user);
    }

    @Test
    public void getAll(){
        List<User> users = userEJB.getAll();
        assertEquals(3, users.size());
    }

    @Test
    public void getUserByEmail(){
        User user = userEJB.getByEmail("f@f2.com");
        assertEquals(user.getEmail(), "f@f2.com");
        assertEquals(user.getUsername(), "a2Username");

        user = userEJB.getByEmail("shouldNotExist");
        assertNull(user);
    }

    @Test
    public void getUserByUsername(){
        User user = userEJB.getByUsername("a2Username");
        assertEquals(user.getUsername(), "a2Username");
        assertEquals(user.getEmail(), "f@f2.com");

        user = userEJB.getByUsername("shouldNotExist");
        assertNull(user);
    }

    @Test
    public void hasComments(){
        User user = userEJB.getById(sampleUserId);
        assertEquals(2, user.getComments().size());
    }

    @Test
    public void addComment(){
        User user = userEJB.getById(sampleUserId);
        int commentsCount = user.getComments().size();

        Comment comment = new Comment();
        comment.setText("someText");
        comment.setCreatedAt(new Date());
        comment.setAuthor(user);
        user.getComments().add(comment);

        transaction.begin();
        commentEJB.persist(comment);
        userEJB.persist(user);
        transaction.commit();

        user = userEJB.getById(sampleUserId);
        assertEquals(commentsCount+1, user.getComments().size());
    }

    @Test
    public void removeComment(){
        User user = userEJB.getById(sampleUserId);
        int commentsCount = user.getComments().size();
        user.getComments().remove(0);

        persistUserInTransaction(user);

        user = userEJB.getById(sampleUserId);
        assertEquals(commentsCount-1, user.getComments().size());
    }

    @Test
    public void hasNews(){
        User user = userEJB.getById(sampleUserId);
        assertEquals(1, user.getNewsList().size());
    }

    @Test
    public void addNews(){
        User user = userEJB.getById(sampleUserId);
        int newsCount = user.getNewsList().size();

        News news = new News();
        news.setAuthor(user);
        news.setTitle("test Title");
        news.setText("some News Text");
        news.setCreatedAt(new Date());
        user.getNewsList().add(news);

        transaction.begin();
        newsEJB.persist(news);
        userEJB.persist(user);
        transaction.commit();

        user = userEJB.getById(sampleUserId);
        assertEquals(newsCount+1, user.getNewsList().size());
    }

    @Test
    public void removeNews(){
        User user = userEJB.getById(sampleUserId);
        int newsCount = user.getNewsList().size();
        user.getNewsList().remove(0);

        persistUserInTransaction(user);

        user = userEJB.getById(sampleUserId);
        assertEquals(newsCount-1, user.getNewsList().size());
    }

    @Test
    public void loginWithEmail() throws Exception {
        User user = userEJB.logIn(email, password);
        assertNotNull(user);
    }

    @Test
    public void loginWithEmailWrongPassword() throws Exception {
        User user = userEJB.logIn(email, "wrongPassword");
        assertNull(user);
    }

    @Test
    public void loginWithUsername() throws Exception {
        User user = userEJB.logIn(username, password);
        assertNotNull(user);
    }

    @Test
    public void loginWithUsernameWrongPassword() throws Exception {
        User user = userEJB.logIn(username, "wrongPassword");
        assertNull(user);
    }

    @Test
    public void loginWrongEmailOrPassword() throws Exception {
        User user = userEJB.logIn("wrong@email.com", password);
        assertNull(user);
    }

    @Test
    public void registerUser() throws Exception {
        User user = new User();
        user.setEmail("testEmail@mail.com");
        user.setUsername("testTheUsername");

        transaction.begin();
        User registeredUser = userEJB.registerUser(user, password);
        transaction.commit();
        assertNotNull(registeredUser);

        User loggedInUser = userEJB.logIn(registeredUser.getUsername(), password);
        assertNotNull(loggedInUser);
    }

    @Test
    public void registerUserEmailAndUsernameTaken() throws Exception {
        User user = new User();
        user.setEmail("f@f2.com");
        user.setUsername("testTheUsername");
        user = userEJB.registerUser(user, password);
        assertTrue(user.getId() <= 0);
    }

    private void persistUserInTransaction(User user){
        transaction.begin();
        userEJB.persist(user);
        transaction.commit();
    }
}
