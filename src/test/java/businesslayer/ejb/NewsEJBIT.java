package businesslayer.ejb;

import entities.Comment;
import entities.News;
import entities.User;
import org.junit.Test;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Fredrik on 08.02.2016.
 */
public class NewsEJBIT extends TestSuite{

    private static final int sampleUserId = 1;
    private static final int sampleNewsId = 1;

    @Test
    public void persist(){
        News news = new News();
        User user = userEJB.getById(sampleUserId);
        news.setAuthor(user);
        news.setTitle("test Title");
        news.setText("some dummy Text");
        news.setCreatedAt(new Date());

        persistNewsInTransaction(news);

        assertTrue(news.getId() > 0);
    }

    @Test
    public void getNewsById(){
        News news = newsEJB.getById(sampleNewsId);
        assertNotNull(news);
        assertEquals("test news content", news.getText());
    }

    @Test
    public void getAllNews(){
        List<News> newsList = newsEJB.getAll();
        assertEquals(6, newsList.size());
    }

    @Test(expected = ConstraintViolationException.class)
    public void newsWithoutTitleShouldFail(){
        News news = newsEJB.getById(sampleNewsId);
        news.setTitle(null);

        try{
            persistNewsInTransaction(news);
        }catch (Exception e){
            throw unwrapConstraintViolationException(e);
        }
    }

    @Test(expected = ConstraintViolationException.class)
    public void newsWithEmptyTitleShouldFail(){
        News news = newsEJB.getById(sampleNewsId);
        news.setTitle("");

        try{
            persistNewsInTransaction(news);
        }catch (Exception e){
            throw unwrapConstraintViolationException(e);
        }
    }

    @Test(expected = ConstraintViolationException.class)
    public void addTooLongTitle() throws Throwable {
        News news = newsEJB.getById(sampleNewsId);
        StringBuilder sb = new StringBuilder(1001);
        for(int i = 0; i < 1001; i++){
            sb.append('1');
        }
        news.setTitle(sb.toString());

        try{
            persistNewsInTransaction(news);
        }catch (Exception e){
            throw unwrapConstraintViolationException(e);
        }
    }

    @Test(expected = ConstraintViolationException.class)
    public void newsWithoutTextShouldFail(){
        News news = newsEJB.getById(sampleNewsId);
        news.setText(null);

        try{
            persistNewsInTransaction(news);
        }catch (Exception e){
            throw unwrapConstraintViolationException(e);
        }
    }

    @Test(expected = ConstraintViolationException.class)
    public void newsWithEmptyTextShouldFail(){
        News news = newsEJB.getById(sampleNewsId);
        news.setText("");

        try{
            persistNewsInTransaction(news);
        }catch (Exception e){
            throw unwrapConstraintViolationException(e);
        }
    }

    @Test(expected = ConstraintViolationException.class)
    public void addNewsWithoutAuthor(){
        News news = newsEJB.getById(sampleNewsId);
        news.setAuthor(null);

        try{
            persistNewsInTransaction(news);
        }catch (Exception e){
            throw unwrapConstraintViolationException(e);
        }
    }

    @Test(expected = ConstraintViolationException.class)
    public void addTooLongText() throws Throwable {
        News news = newsEJB.getById(sampleNewsId);
        StringBuilder sb = new StringBuilder(90001);
        for(int i = 0; i < 90001; i++){
            sb.append('1');
        }
        news.setText(sb.toString());

        try{
            persistNewsInTransaction(news);
        }catch (Exception e){
            throw unwrapConstraintViolationException(e);
        }
    }

    @Test
    public void hasComments(){
        News news = newsEJB.getById(sampleNewsId);
        assertEquals(4, news.getComments().size());
    }

    @Test
    public void addComment(){
        News news = newsEJB.getById(sampleNewsId);
        int commentCount = news.getComments().size();

        Comment comment = new Comment();
        comment.setText("some Text");
        comment.setCreatedAt(new Date());
        comment.setAuthor(userEJB.getById(sampleUserId));
        comment.setNews(news);
        news.getComments().add(comment);

        transaction.begin();
        commentEJB.persist(comment);
        newsEJB.persist(news);
        transaction.commit();

        news = newsEJB.getById(sampleNewsId);
        assertEquals(commentCount+1, news.getComments().size());
    }

    @Test
    public void removeComment(){
        News news = newsEJB.getById(sampleNewsId);
        int commentCount = news.getComments().size();
        news.getComments().remove(news.getComments().iterator().next());

        persistNewsInTransaction(news);

        news = newsEJB.getById(sampleNewsId);
        assertEquals(commentCount-1, news.getComments().size());
    }

    @Test
    public void hasVotes(){
        News news = newsEJB.getById(sampleNewsId);
        assertEquals(2, news.getUpVotes().size());
        assertEquals(2, news.getDownVotes().size());
    }

    @Test
    public void addVotes(){
        User user1 = userEJB.getById(3);
        User user2 = userEJB.getById(2);
        News news = newsEJB.getById(sampleNewsId);
        int upVoteCount = news.getUpVotes().size();
        int downVoteCount = news.getDownVotes().size();

        transaction.begin();
        newsEJB.upVote(news, user1);
        newsEJB.downVote(news, user2);
        transaction.commit();

        news = newsEJB.getById(sampleNewsId);
        assertEquals(upVoteCount+1, news.getUpVotes().size());
        assertEquals(downVoteCount+1, news.getDownVotes().size());
    }

    @Test
    public void removeVotes(){
        News news = newsEJB.getById(sampleNewsId);
        int upVoteCount = news.getUpVotes().size();
        int downVoteCount = news.getDownVotes().size();
        news.getUpVotes().remove(0);
        news.getDownVotes().remove(0);

        persistNewsInTransaction(news);

        news = newsEJB.getById(sampleNewsId);
        assertEquals(upVoteCount-1, news.getUpVotes().size());
        assertEquals(downVoteCount-1, news.getDownVotes().size());
    }

    @Test
    public void getAllNewestFirst(){
        List<News> news = newsEJB.getAllNewestFirst();
        if(news.size() < 2) fail("no news to test!");

        for(int i = 1; i < news.size(); i++){
            long newestDate = news.get(i-1).getCreatedAt().getTime();
            long oldestDate = news.get(i).getCreatedAt().getTime();
            assertTrue(newestDate >= oldestDate);
        }
    }

    @Test
    public void mostVotesOfToday(){
        changeCreatedAtDateToToday(newsEJB.getAll()); //keeping tests up to date
        List<News> news = newsEJB.mostUpVotesOfToday();
        if(news.size() < 2) fail("no news to test!");

        for(int i = 1; i < news.size(); i++){
            int mostUpVotesCount = news.get(i-1).getUpVotes().size();
            int secondMostUpVotesCount = news.get(i).getUpVotes().size();
            assertTrue(mostUpVotesCount >= secondMostUpVotesCount);
        }
    }

    @Test
    public void mostCommentsOfToday(){
        changeCreatedAtDateToToday(newsEJB.getAll()); //keeping tests up to date
        List<News> news = newsEJB.mostCommentsOfToday();
        if(news.size() < 2) fail("no news to test!");

        for(int i = 1; i < news.size(); i++){
            int mostCommentsCount = news.get(i-1).getComments().size();
            int secondMostCommentsCount = news.get(i).getComments().size();
            assertTrue(mostCommentsCount >= secondMostCommentsCount);
        }
    }

    @Test
    public void totalNumberOfNews(){
        int totalNumberOfNews = newsEJB.totalNumberOfNews();
        assertEquals(6, totalNumberOfNews);
    }

    @Test
    public void getAvgNewsPerDay(){
        double avg = newsEJB.getAvgNewsPerDay();
        assertEquals(3.0, avg, 0.009);
    }

    private void changeCreatedAtDateToToday(List<News> newsList){
        transaction.begin();
        for(News news : newsList){
            news.setCreatedAt(new Date());
            newsEJB.persist(news);
        }
        transaction.commit();
    }

    private void persistNewsInTransaction(News news){
        transaction.begin();
        newsEJB.persist(news);
        transaction.commit();
    }
}
