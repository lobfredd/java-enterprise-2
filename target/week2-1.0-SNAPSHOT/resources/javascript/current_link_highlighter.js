/**
 * Created by Fredrik on 12.03.2016.
 *
 */

$(document).ready(function(){
    $('a').each(function() {
        var href = $(this).prop('href').split(';')[0].split('?')[0]; //excluding url params
        var location = window.location.href.split(';')[0].split('?')[0]; //excluding url params
        if (href == location)  {
            $(this).addClass('current');
        }
    });
});